# A Docker project

Builds a docker image that runs node and hosts a web-server on port 8080, displaying cowsay and linking to a docker image repository

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#Interesting Docker Usege)
- [Maintainers](#maintainers)

## Requirements

Requires `Docker`.

## Usage

Uses Docker
```sh
# Build the image in one of the two following ways:
# 1. After cloning
$ docker build -t hannes-docker:1.0 .
# 2. Directly from repository
$ docker build https://gitlab.com/HannesRingblom/ass2_2.git#main -t hannes-docker:1.0
# Run the container
$ docker run --rm -p 8080:3000 hannes-docker:1.0
```

## Interesting Docker Usage

### Chosen topic: HandBrake

HandBrake is a tool for converting video from nearly any format to a selection of modern, widely supported codecs. HandBrake can be used fully automated, supply files and they are processed by HandBrake without further user interaction. 


## Maintainers

[Hannes Ringblom @HannesRingblom](https://gitlab.com/HannesRingblom)

