const express = require("express")
const app = express()
const { PORT = 3000 } = process.env
const path = require("path")

app.use( express.static( path.join(__dirname, "public" )))

app.get("/", function(req, res){
    return res.sendFile(path.join(__dirname, "public", "index.html"))
})

app.listen(PORT, function(){
    console.log("Server hosted on specified port")
})
